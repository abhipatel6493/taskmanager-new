echo -e '\n ===> GET ALL LOGS \n'
rm -rf logs
mkdir logs
docker cp tm:/logs/application.log ./logs
docker cp tm:/logs/appSummary.log ./logs
docker cp tm:/logs/appErrors.log ./logs
docker cp tm:/logs/appMethodCallTrace.log ./logs
docker cp tm:/logs/appMethodExecutionTimeTrace.log ./logs

