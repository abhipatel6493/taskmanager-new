echo -e '\n ===> DOCKER_COMPOSE DOWN \n' 
docker-compose down

echo -e '\n ===> DOCKER KILL ALL CONTAINERS \n'
docker ps -a -q | xargs docker kill

echo -e '\n ===> DOCKER REMOVE ALL IMAGES \n'
docker system prune -a -f

echo -e '\n ===> DOCEKR REMOVE ALL VOLUMES \n'
if [[ $(docker volume ls |awk 'NR>1 {print $2}') ]]
then
   docker volume rm $(docker volume ls |awk 'NR>1 {print $2}')
else
   echo 'No volumes found'
fi

echo -e '\n ===> DOCKER REMOVE ALL NETWORKS \n'
docker network ls -q | xargs docker network rm

echo -e '\n ===> DOCKER CLEAR LOGS \n'
cd ../taskmanager
rm -rf logs build
