echo -e '\n ===> DOCKER COMPOSE DOWN \n'
docker-compose down

echo -e '\n ===> DOCKER REMOVE UNUSED IMAGES \n'
docker system prune -f

echo -e '\n ===> DOCKER REMOVE UNUSED VOLUMES \n'
docker volume prune -f

echo -e '\n ===> DOCKER REMOVE UNUSED NETWORKS \n'
docker network prune -f

echo -e '\n ===> DOCKER KILL ALL CONTAINERS \n'
docker ps -a -q | xargs docker kill

echo -e '\n ===> CLEAR LOGS AND BUILD \n'
cd ../taskmanager
rm -rf logs build
