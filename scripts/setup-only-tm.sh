cd ../taskmanager
echo -e '\n ===> GRADLE BUILD TASKMANAGER \n'
gradle clean build

echo -e '\n ===> DOCKER IMAGE BUILD TASKMANAGER \n'
docker build --no-cache -t abhidockerhub/tm:latest .
