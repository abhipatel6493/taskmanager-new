echo -e '\n ===> BUILDING COMMON-LIBRARY \n'
cd ../../libraries/Common-Library/
gradle clean install 


cd ../../tmconfigserver
echo -e '\n ===> BUILDING TM CONFIG SERVER'
gradle clean build

echo -e '\n ===> DOCKER IMAGE BUILD TM CONFIG SERVER \n'
docker build --no-cache -t abhidockerhub/tmconfigserver:latest .


cd ../eureka
echo -e '\n ===> BUILDING EUREKA SERVER'
gradle clean build

echo -e '\n ===> DOCKER IMAGE BUILD EUREKA SERVER \n'
docker build --no-cache -t abhidockerhub/eureka:latest .



cd ../adminserver
echo -e '\n ===> BUILDING ADMIN SERVER'
gradle clean build

echo -e '\n ===> DOCKER IMAGE BUILD ADMIN SERVER \n'
docker build --no-cache -t abhidockerhub/adminserver:latest .



cd ../taskmanager-new/taskmanager
echo -e '\n ===> GRADLE BUILD TASKMANAGER \n'
gradle build

echo -e '\n ===> DOCKER IMAGE BUILD TASKMANAGER \n'
docker build --no-cache -t abhidockerhub/tm:latest .
