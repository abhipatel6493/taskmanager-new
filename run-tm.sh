echo -e '\n ===> CHANGING PERMISSIONS \n'
chmod -R 777 *

echo -e '\n ===> CONVERTING FILES TO LINUX \n'
dos2unix *

cd scripts
./clean-tm.sh
./setup-tm.sh
./start-tm.sh
