package com.abhi.tm.commonconfig;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@Log4j2
@RefreshScope
public class CommonConfigController {

    @Autowired
    CommonConfig commonConfig;

    @Value("${name: shek}")
    private String name;

    @GetMapping("/public/common-config")
    public Flux<?> getCommonConfig(){
        log.info("DATA FROM CONFIG SERVER: " + name + " = " + commonConfig.getName());
        return Flux.just("DATA FROM CONFIG SERVER: " + name);
    }
}
