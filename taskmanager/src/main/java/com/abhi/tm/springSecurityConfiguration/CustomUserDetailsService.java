package com.abhi.tm.springSecurityConfiguration;

import com.abhi.tm.userservice.dao.User;
import com.abhi.tm.userservice.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService{

	@Autowired
	UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		Optional<User> users = userRepository.findByUserName(userName);
		users.orElseThrow(() -> new UsernameNotFoundException("User " + userName + " not found.."));
		CustomUserDetails user = users.map(CustomUserDetails::new).get();
		return user;
	}

}
