package com.abhi.tm.designpattren.creational.abstractfactory;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Component;

@Component
@Data
@EqualsAndHashCode(callSuper=false)
public class Server extends Computer{
    private String name;
}
