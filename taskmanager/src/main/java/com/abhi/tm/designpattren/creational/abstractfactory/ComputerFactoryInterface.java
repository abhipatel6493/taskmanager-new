package com.abhi.tm.designpattren.creational.abstractfactory;

public interface ComputerFactoryInterface {

    Computer getComputer();
}
