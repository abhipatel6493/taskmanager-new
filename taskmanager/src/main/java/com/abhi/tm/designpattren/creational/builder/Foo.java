package com.abhi.tm.designpattren.creational.builder;

import lombok.Data;

@Data
public class Foo {
    private String name;
    private int age;

   public void setName(String name){
        this.name = name;
    }

    public void setAge(int age){
        this.age = age;
    }

    public static class FooBuilder{

        private String name;
        private int age;

        public FooBuilder setName(String name){
            this.name  = name;
            return this;
        }

       public FooBuilder setAge(int age){
           this.age  = age;
           return this;
       }

       public Foo build(){
            return new Foo();
       }
    }
}
