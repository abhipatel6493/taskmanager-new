package com.abhi.tm.designpattren.creational.prototype;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class Prototype implements  Cloneable{
    Set<Data> list = new HashSet<>();

    public void load(){
        Data data = new Data();
        this.list = new HashSet<>();
        this.list.add(data);
    }

    @Override
    public List<Data> clone(){
        List<Data> list1 = new ArrayList<>();
        list1.addAll(this.list);
        return list1;
    }
}
