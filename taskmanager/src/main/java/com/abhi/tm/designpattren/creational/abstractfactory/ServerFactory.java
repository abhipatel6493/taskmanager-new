package com.abhi.tm.designpattren.creational.abstractfactory;

public class ServerFactory implements ComputerFactoryInterface {
    @Override
    public Computer getComputer() {
        return new Server();
    }
}
