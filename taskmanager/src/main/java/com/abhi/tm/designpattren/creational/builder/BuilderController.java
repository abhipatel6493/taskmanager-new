package com.abhi.tm.designpattren.creational.builder;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class BuilderController {

    @GetMapping("/public/builderDp")
    private void builderDp(){
       log.info("Builder DP: " + new Foo.FooBuilder().setName("abhi").setAge(27));
    }
}
