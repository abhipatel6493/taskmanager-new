package com.abhi.tm.designpattren.creational.abstractfactory;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class FactoryController {

    @GetMapping("/public/factoryDp{name}")
    private void factoryDesignPattren(@PathVariable String name){
        log.info("ABSTRACT FACTORY PATTREN, PC: " + ComputerFactory.getComputer(new PcFactory()));
        log.info("ABSTRACT FACTORY PATTREN, SERVER: " + ComputerFactory.getComputer(new ServerFactory()));
    }
}
