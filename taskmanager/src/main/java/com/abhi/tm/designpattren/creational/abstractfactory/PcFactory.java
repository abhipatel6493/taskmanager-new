package com.abhi.tm.designpattren.creational.abstractfactory;

public class PcFactory implements ComputerFactoryInterface {
    @Override
    public Computer getComputer() {
        return new PC();
    }
}
