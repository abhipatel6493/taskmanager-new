package com.abhi.tm.designpattren.creational.singleton;

import org.springframework.stereotype.Component;

@Component
public class TmSingletonClass {

    private TmSingletonClass(){}

    private static final TmSingletonClass tmSingletonBean = new TmSingletonClass();

    public static synchronized TmSingletonClass getTmSingletonClass(){
        return tmSingletonBean;
    }

}
