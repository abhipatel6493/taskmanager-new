package com.abhi.tm.designpattren.creational.abstractfactory;

public class ComputerFactory {

    public static Computer getComputer(ComputerFactoryInterface computerFactory){
        return computerFactory.getComputer();
    }

}
