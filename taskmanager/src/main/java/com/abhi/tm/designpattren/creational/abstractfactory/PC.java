package com.abhi.tm.designpattren.creational.abstractfactory;

import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@EqualsAndHashCode(callSuper=false)
public class PC extends Computer{
    private String name;
}
