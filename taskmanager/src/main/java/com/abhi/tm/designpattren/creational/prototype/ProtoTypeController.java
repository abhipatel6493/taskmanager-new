package com.abhi.tm.designpattren.creational.prototype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProtoTypeController {

    @Autowired
    Prototype prototype;

    @GetMapping("/public/prototype")
    private void protoTypeDp(){
        List<Data> prototype1 = prototype.clone();
    }
}
