package com.abhi.tm.multithreading;

import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class ScheduledES {

    public void scheduledESerAtFixedRate(){
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);
        executorService.scheduleAtFixedRate(()->System.out.println("scheduledESerAtFixedRate"), 1, 2, TimeUnit.SECONDS);
    }

    public void scheduledESerAtFixedDelay(){
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);
        executorService.scheduleWithFixedDelay(()->System.out.println("scheduledESerAtFixedRate"), 1, 2, TimeUnit.SECONDS);
    }

}
