package com.abhi.tm.multithreading;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.concurrent.*;
import java.util.stream.IntStream;

@Service
@Log4j2
public class FutureData {

    public void asyncCal(){
        ExecutorService executorService =  Executors.newFixedThreadPool(10);

        Future future = executorService.submit(() -> System.out.println("Trhead : " + Thread.currentThread().getName()));

        log.info(future.isDone());

        try {
            log.info("Data" + future.get(10, TimeUnit.SECONDS).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }
}
