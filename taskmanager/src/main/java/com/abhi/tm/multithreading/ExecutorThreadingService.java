package com.abhi.tm.multithreading;

import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.stream.IntStream;

@Service
public class ExecutorThreadingService {

    public void asyncCal() throws InterruptedException {
        ExecutorService executorService =  Executors.newFixedThreadPool(2);

        CountDownLatch count = new CountDownLatch(3);
        IntStream.range(0, 3)
                .forEach(i -> {
                    executorService.submit(() -> {
                        System.out.println("Trhead : " + Thread.currentThread().getName());
                        count.countDown();
                    });
                });
        count.await();
        System.out.println("DONE1==>");
        executorService.shutdown();

        ExecutorService executorService1 =  Executors.newFixedThreadPool(5);
        IntStream.range(0, 10)
                .forEach(i -> {
                    executorService1.submit(() -> System.out.println("Trhead ==== : " + Thread.currentThread().getName()));
                });

        executorService1.shutdown();
    }
}
