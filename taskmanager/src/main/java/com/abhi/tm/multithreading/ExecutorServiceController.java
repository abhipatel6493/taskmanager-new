package com.abhi.tm.multithreading;

import com.abhi.tm.multithreading.ExecutorThreadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExecutorServiceController {

    @Autowired
    ExecutorThreadingService executorThreadingService;

    @Autowired
    ScheduledES scheduledES;

    @Autowired
    FutureData futureData;

    @GetMapping("/public/executeConcurrentThreads")
    private void executorThreads() throws InterruptedException {
        executorThreadingService.asyncCal();
        scheduledES.scheduledESerAtFixedRate();
        scheduledES.scheduledESerAtFixedDelay();
        futureData.asyncCal();
    }
}
