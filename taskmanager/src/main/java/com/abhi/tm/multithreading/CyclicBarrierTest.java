package com.abhi.tm.multithreading;

import org.springframework.stereotype.Service;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

@Service
public class CyclicBarrierTest {

    class RunA implements Runnable{

        RunA(CyclicBarrier cb){
            this.b=cb;
        }
        public CyclicBarrier b;

        @Override
        public void run() {
            try {
                Thread.sleep(1000);
                System.out.println("A...");
                this.b.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
            System.out.println("DONE");
        }
    }

    public void cyclicBarierTest(){
        CyclicBarrier cb = new CyclicBarrier(2, () -> System.out.println("Barier.."));
        ExecutorService executorService =  Executors.newFixedThreadPool(5);
        RunA r = new RunA(cb);
        IntStream.range(0, 2)
                .forEach(i -> {
                    executorService.submit(r);
                });
        executorService.shutdown();
    }
}
