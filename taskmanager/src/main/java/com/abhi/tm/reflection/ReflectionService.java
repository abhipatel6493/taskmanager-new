package com.abhi.tm.reflection;

import com.abhi.tm.learn.service.TmService;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class ReflectionService {

    public void reflectionExample(){
        Arrays.stream(TmService.class.getMethods())
                .forEach(method -> System.
                        out.println(method.getName()));
    }
}
