package com.abhi.tm;

import com.abhi.tm.publisher.TmKafkaTopics;
import com.abhi.tm.springSecurityConfiguration.SessionListener;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

@SpringBootApplication
@EnableScheduling
@ComponentScan({"com.abhi.tm", "com.abhi.lib"})
@EnableBinding({TmKafkaTopics.class})
@EnableMongoRepositories(basePackages = "com.abhi.tm")
@EnableDiscoveryClient
@EnableJpaRepositories("com.abhi.tm")
@EnableFeignClients
public class TaskmanagerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(TaskmanagerApplication.class, args);
	}

	// Session listner for create and delete
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);
		servletContext.addListener(new SessionListener());
	}

	// Password encryptor and decryptor.
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	// ModelMapper used for convertin DTO to entity and viceversa.
	@Bean
	public ModelMapper modelMapper() {
		System.out.println("======== Creating Model Map object ==========");
		return new ModelMapper();
	}
}
