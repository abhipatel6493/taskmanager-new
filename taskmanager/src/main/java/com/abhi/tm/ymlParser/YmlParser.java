package com.abhi.tm.ymlParser;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "data")
public class YmlParser {

    private String name;

    private int age;
}
