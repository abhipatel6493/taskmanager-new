package com.abhi.tm.scheduler;

import com.abhi.tm.constants.Constants;
import com.abhi.tm.learn.dto.CommonData;
import com.abhi.tm.enums.TmEnum;
import lombok.extern.log4j.Log4j2;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Log4j2
public class TmScheduler {

    @Autowired
    Environment environment;

    @Autowired
    CommonData commonData;

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job job;

    @PostConstruct
    public void onStartup() {
        doWork();
    }

    private void doWork() {
        log.info("STARTED TASKMANAGER APPLICATION " + TmEnum.SCHEDULER);
        commonData.setDeveloper(environment.getProperty(Constants.DEVELOPER));
    }

    @Scheduled(fixedDelay = 2000)
    public void setUp() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        //triggering batch job at the initial
        JobParameters jobParameters = new JobParameters();
        JobExecution jobExecution = jobLauncher.run(job, jobParameters);

        while(jobExecution.isRunning()){
            log.info("BATCH JOB IS RUNNING...");
        }
        log.info("BATCH JOB STATUS: " + jobExecution.getStatus());
    }

}
