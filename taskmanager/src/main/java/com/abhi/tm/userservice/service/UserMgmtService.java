package com.abhi.tm.userservice.service;

import java.util.List;

import com.abhi.tm.userservice.common.BusinessServiceException;
import com.abhi.tm.userservice.common.DataAccessException;
import com.abhi.tm.userservice.dao.User;
import com.abhi.tm.userservice.dto.UserDto;

/**
 * @author Abhishek Patel M N Jan 17, 2018
 */
public interface UserMgmtService {

	void addUser(User user) throws BusinessServiceException, DataAccessException;

	List<UserDto> getAllUsers() throws BusinessServiceException, Exception;

	UserDto getUserByUserId(Integer UserId) throws BusinessServiceException, DataAccessException;

	void deleteUser(Integer userId) throws BusinessServiceException, DataAccessException;

	void updateUser(User user) throws BusinessServiceException, DataAccessException;
		
}
