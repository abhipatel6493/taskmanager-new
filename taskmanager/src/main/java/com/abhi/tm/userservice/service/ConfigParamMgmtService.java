package com.abhi.tm.userservice.service;

import java.util.List;

import com.abhi.tm.userservice.common.BusinessServiceException;
import com.abhi.tm.userservice.common.DataAccessException;
import com.abhi.tm.userservice.dao.ConfigParam;
import com.abhi.tm.userservice.dto.ConfigParamDto;


/**
 * @author Abhishek Patel M N Jan 17, 2018
 */
public interface ConfigParamMgmtService {

	void addConfigParam(ConfigParamDto configParamDto) throws BusinessServiceException, DataAccessException;

	List<ConfigParamDto> getAllConfigParams() throws BusinessServiceException, Exception;

	ConfigParamDto getConfigParamByConfigParamId(Integer configParamId) throws BusinessServiceException, DataAccessException;

	void deleteConfigParam(Integer configParamId) throws BusinessServiceException, DataAccessException;

	void updateConfigParam(ConfigParamDto configParamDto) throws BusinessServiceException, DataAccessException;

	ConfigParam findByParamName(String configParamName);
	
}
