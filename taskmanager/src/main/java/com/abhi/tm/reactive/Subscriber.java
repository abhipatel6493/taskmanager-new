package com.abhi.tm.reactive;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.concurrent.Flow;

@Log4j2
@Component
public class Subscriber<T> implements Flow.Subscriber<T> {

    private Flow.Subscription subscription;

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
       log.info("Subscribed..");
       subscription = subscription;
       subscription.request(1);
    }

    @Override
    public void onNext(Object o) {
        log.info("Data.. " + o.toString());
        subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        log.info("Error: " + throwable.getMessage());
    }

    @Override
    public void onComplete() {
        log.info("DONE.. ");
    }
}
