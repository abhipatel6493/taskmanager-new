package com.abhi.tm.reactive;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.concurrent.SubmissionPublisher;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

@RestController
@Log4j2
public class ReactiveStreamController {

    @Autowired
    Subscriber subscriber;

    @GetMapping("/public/reactive/java9")
    public void java9() throws InterruptedException {

        SubmissionPublisher<String> publisher = new SubmissionPublisher<>();
        publisher.subscribe(subscriber);

        IntStream.range(0, 10)
                .forEach(i -> {
                    log.info("Submitting to publisher: " + i);
                    publisher.submit(String.valueOf(Integer.valueOf(i)));
                });

        Thread.sleep(1000);
        publisher.close();
    }

    @GetMapping(value = "/public/reactive/reactor", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<String> reactor() throws InterruptedException {

        AtomicInteger c = new AtomicInteger();
        return Flux.<String>generate(sink -> sink.next("abhi " + c.getAndIncrement() + "\n")).take(10).delayElements(Duration.ofSeconds(1));

    }
}