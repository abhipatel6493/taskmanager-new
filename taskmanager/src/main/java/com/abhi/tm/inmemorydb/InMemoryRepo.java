package com.abhi.tm.inmemorydb;

import com.abhi.tm.learn.dto.Support;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InMemoryRepo extends JpaRepository<Support, String> {
}
