package com.abhi.tm.inmemorydb;

import com.abhi.tm.learn.dto.Support;
import com.abhi.tm.inmemorydb.InMemoryService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InMemoryDbController {

    private InMemoryService inMemoryService;

    @PostMapping("/addDataToInMemoryDb")
    private void addDataToInMemoryDb(@RequestBody Support data){
        inMemoryService.addDataToInMemoryDb(data);
    }

}
