package com.abhi.tm.inmemorydb;

import com.abhi.tm.learn.dto.Support;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InMemoryService {

    @Autowired
    private InMemoryRepo inMemoryReop;

    public void addDataToInMemoryDb(Support data){
        inMemoryReop.save(data);
    }
}
