package com.abhi.tm.publisher;


import com.abhi.tm.learn.dto.Support;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class KafkaPublisher {

    @Autowired
    TmKafkaTopics tmKafkaTopics;

    @StreamListener(TmKafkaTopics.tmInput)
    public void consumeFromKafka(Message message){
        log.info(message.getPayload().toString());
    }

    public void publishToKafka(Support data){
        tmKafkaTopics.tmOutbound().send(MessageBuilder.withPayload(data).build());
    }
}
