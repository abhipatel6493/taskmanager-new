package com.abhi.tm.publisher;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface TmKafkaTopics {

    String tmInput = "input";
    String tmOutput = "output";

    @Input(tmInput)
    SubscribableChannel tmInbound();

    @Output(tmOutput)
    MessageChannel tmOutbound();

}
