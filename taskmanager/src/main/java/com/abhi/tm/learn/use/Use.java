package com.abhi.tm.learn.use;

import java.util.concurrent.*;
import java.util.stream.IntStream;

public class Use {

    public static void main(String[] args) throws ExecutionException, InterruptedException {


        CyclicBarrier cb = new CyclicBarrier(2, () -> System.out.println("Barier.."));
        ExecutorService executorService =  Executors.newFixedThreadPool(2);

        RunA r = new RunA();
        r.b = cb;
        IntStream.range(0, 10)
                .forEach(i -> {
                    executorService.submit(r);
                });
        executorService.shutdown();

    }

}
