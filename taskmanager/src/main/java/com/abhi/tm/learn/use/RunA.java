package com.abhi.tm.learn.use;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

class RunA implements Runnable{

    static CyclicBarrier b;

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            System.out.println("A...");
            int wait = this.b.await();
            if(wait == 0){
                System.out.println("GOOOOOOOOO====");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        System.out.println("DONE");
    }
}