package com.abhi.tm.learn.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class CommonData {

    @Value("${PATH}")
    private String systemPathVar;

    // Setting from docker env variable using scheduler
    private String developer;

}
