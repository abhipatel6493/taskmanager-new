package com.abhi.tm.learn.use;

@FunctionalInterface
interface FunInterface {

    void doo();

    default void def1(){
        System.out.println("default method");
    }

    static  void sta1(){
        System.out.println("static method");
    }
}
