package com.abhi.tm.learn.controller;

import com.abhi.tm.learn.dto.CommonData;
import com.abhi.tm.learn.dto.Support;
import com.abhi.tm.reflection.ReflectionService;
import com.abhi.tm.learn.service.TmService;
import com.abhi.tm.ymlParser.YmlParser;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@RestController
@Log4j2
public class TmController {

    @Autowired
    @Qualifier("appSummaryLogger")
    private Logger summaryLogger;

    @Autowired
    private CommonData commonData;

    @Autowired
    private Support support;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private TmService tmService;

    @Autowired
    private ReflectionService reflectionService;

    @Autowired
    private YmlParser ymlParser;

    @GetMapping("/rest/{name}")
    private ResponseEntity<String> foo(@PathVariable String name){

        log.info("YmlParsed bean data: " + ymlParser.toString());

        log.info("systemPathVar: " + commonData.getSystemPathVar());
        log.info("Welcome " + name);
        log.info("Developer: " + commonData.getDeveloper());

        summaryLogger.info("aaaaaa");
        log.debug("This is a debug message");
        log.info("This is an info message");
        log.warn("This is a warn message");
        log.error("This is an error message");
        log.fatal("This is a fatal message");

        Resource resource = resourceLoader.getResource("classpath:data.txt");
        try(InputStream inputStream = resource.getInputStream())
        {
            byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
            String data = new String(bdata, StandardCharsets.UTF_8);
            log.info("Data from resource folder, i.e classpath file: " + data);
        }
        catch (IOException e)
        {
            log.error("IOException", e);
        }

        log.info("Generics: " + tmService.goo("abhishek"));
        support.setName("Patel");
        log.info("Generics: " + tmService.goo(support));
        tmService.koo(commonData, "abhi", 2);
        tmService.cal(3);
        tmService.ref();

        reflectionService.reflectionExample();

        return new ResponseEntity<String>("Welcome " + name + ", Developer: " + commonData.getDeveloper(), HttpStatus.OK);
    }

    @PostMapping("/rest/pub")
    private void publishToKafka(@RequestBody @Valid Support data){
        tmService.publishToKafka(data);
    }
}
