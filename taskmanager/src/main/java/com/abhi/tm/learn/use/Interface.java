package com.abhi.tm.learn.use;

public interface Interface {
    void doo();

    default void def2() {
        System.out.println("default 2");
    }

    static  void sta1(){
        System.out.println("static 2 method");
    }
}
