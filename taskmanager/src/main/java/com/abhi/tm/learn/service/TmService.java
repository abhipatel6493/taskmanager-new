package com.abhi.tm.learn.service;

import com.abhi.lib.Library;
import com.abhi.lib.annotation.MethodTrace;
import com.abhi.tm.learn.dto.CommonData;
import com.abhi.tm.learn.dto.Support;
import com.abhi.tm.funInterface.Calculate;
import com.abhi.tm.publisher.KafkaPublisher;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.*;
import java.time.format.DateTimeFormatter;

@Service
@Log4j2
public class TmService {

    @Autowired
    private CommonData commonData;

    @Autowired
    private Library library;

    @Autowired
    private KafkaPublisher kafkaPublisher;

    private void java(){
        System.out.println(LocalTime.now());
        System.out.println(LocalDate.now());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM d yyyy");

        LocalDate localDate = LocalDate.of(2020, Month.APRIL, 6);

        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.systemDefault());

        System.out.println(zonedDateTime.format(formatter));
    }

    public <T> T goo(T t){
        commonData.getSystemPathVar();
        boolean b = library.someLibraryMethod();
        System.out.println(t.toString());
        return t;
    }

    public void ref(){
        var support = new Support();
        var cls = support.getClass();
        log.info(cls.getSimpleName());
    }

    public void koo(CommonData commonData, String name, int i){
        commonData.getSystemPathVar();
    }

    @MethodTrace
    public  int cal(int i){
            Calculate c = x -> x*x;
        return c.cal(i);
    }

    public void publishToKafka(Support data){
        kafkaPublisher.publishToKafka(data);
    }
}
