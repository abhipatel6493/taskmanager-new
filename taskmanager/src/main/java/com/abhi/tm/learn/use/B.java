package com.abhi.tm.learn.use;

public class B {
    public void qoo(FunInterface funInterface){
        funInterface.doo();
    }

    public void def3(){
        System.out.println("default method");
    }

    public void def2(){
        System.out.println("default method 2");
    }

    static  void sta1(){
        System.out.println("static method");
    }
}
