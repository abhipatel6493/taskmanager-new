package com.abhi.tm.learn.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Component
@Data
@Entity
public class Support {

    @Id
    private Long id;

    @NotBlank
    private String name;
}
