package com.abhi.tm.springbatch;

import lombok.Data;

@Data
public class BatchData {
    private String data;
}
