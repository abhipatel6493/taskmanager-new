package com.abhi.tm.springbatch;

import lombok.extern.log4j.Log4j2;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class BatchProcessor implements ItemProcessor<BatchData, BatchData> {
    @Override
    public BatchData process(BatchData item) throws Exception {
       log.info("PROCESSING BATCH DATA..");
       return item;
    }
}
