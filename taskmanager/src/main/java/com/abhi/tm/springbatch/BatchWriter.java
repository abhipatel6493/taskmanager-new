package com.abhi.tm.springbatch;

import lombok.extern.log4j.Log4j2;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
public class BatchWriter implements ItemWriter<BatchData> {
    @Override
    public void write(List<? extends BatchData> items) throws Exception {
        log.info("WRITING BATCH DATA.. " + items.toString());
    }
}
