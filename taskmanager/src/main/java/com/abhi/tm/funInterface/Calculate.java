package com.abhi.tm.funInterface;

@FunctionalInterface
public interface Calculate {
    int cal(int i);
}
