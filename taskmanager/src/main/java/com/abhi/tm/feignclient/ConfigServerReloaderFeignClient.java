package com.abhi.tm.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "ConfigServerReloaderFeignClient", url = "localhost:" + "${server.port}" + "/actuator/refresh")
public interface ConfigServerReloaderFeignClient {

    @PostMapping
    void loadConfigServerData();

}
