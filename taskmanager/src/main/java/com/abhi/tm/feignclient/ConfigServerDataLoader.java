package com.abhi.tm.feignclient;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class ConfigServerDataLoader {

    @Autowired
    ConfigServerReloaderFeignClient configServerReloaderFeignClient;

    @Scheduled(fixedDelay = 5000)
    private void loadDataFromConfigServer(){
        log.info("RELOADED DATA FROM CONFIG SERVER...");
        configServerReloaderFeignClient.loadConfigServerData();
    }
}
